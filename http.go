package _core

import (
	"net/http"
	"encoding/json"
	"fmt"
)

type ResponseObject struct {
	code int
	data interface{}
}

func ThrowError(code int) {
	panic(ResponseObject{
		code: code,
	})
}

func Deleted() {
	ThrowError(http.StatusNoContent)
}

func Created(data interface{}) {
	panic(ResponseObject{
		code: http.StatusCreated,
		data: data,
	})
}

func SendResponse(data interface{}) {
	panic(ResponseObject{
		code: http.StatusOK,
		data: data,
	})
}

func writeResponse(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json")

	//Recovering thrown data
	panicData := recover()
	if panicData == nil {
		fmt.Println("No panic data")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	//Parsing recover() data into ResponseObject
	responseObject, ok := panicData.(ResponseObject)
	if !ok {
		fmt.Println("Unparsable panic data:")
		fmt.Println(panicData)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	//Parsing response body as JSON
	responseText, err := json.Marshal(responseObject.data)
	if err != nil {
		fmt.Println("Error encoding Response data")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	//Writing response string
	w.WriteHeader(responseObject.code)
	w.Write(responseText)
}

func GetRequestData(r *http.Request, item interface{}) {
	err := json.NewDecoder(r.Body).Decode(&item)
	if err != nil {
		ThrowError(http.StatusBadRequest)
	}
}
