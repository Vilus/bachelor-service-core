package _core

import "github.com/dgrijalva/jwt-go"

type Claims struct {
	Id    string `json:"id"`
	Email string `json:"email"`
	Exp   int64 `json:"exp"`
	jwt.StandardClaims
}