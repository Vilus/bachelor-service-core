package _core

import (
	"net/http"
	"encoding/json"
	"fmt"
	"crypto/rsa"
)

type PublicKey struct {
	Id      int
	Content *rsa.PublicKey
}

var keys []PublicKey
func getKeys() () {

	resp, err := http.Get("http://localhost:8080/keys")
	if err != nil {
		fmt.Println(err)
	}

	defer resp.Body.Close()

	var keysFromAPI []PublicKey
	err = json.NewDecoder(resp.Body).Decode(&keysFromAPI)
	if err != nil {
		fmt.Println(err)
	}

	keys = keysFromAPI
}

func GetKeyById(Id int, allowRefresh bool) (*rsa.PublicKey, bool) {
	maxId :=0

	for _, element := range keys {
		if maxId < element.Id{
			maxId = element.Id
		}
		if element.Id == Id {
			return element.Content, true
		}
	}

	if maxId < Id && allowRefresh{
		getKeys() //getting keys from API
		return GetKeyById(Id, false)
	}
	var item *rsa.PublicKey
	return item, false
}
