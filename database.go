package _core

import (
	"fmt"
	"gopkg.in/mgo.v2"
)
const database = "db"

var mongoDatabase *mgo.Database
var mongoSession *mgo.Session

func initDatabase() {

	fmt.Println("Connecting to MongoDB!")
	var err1 error
	mongoSession, err1 = mgo.Dial("admin:admin@192.168.99.100:27017")
	if err1 != nil {
		panic(err1)
	}

}

func GetMongoCollection(collection string) *mgo.Collection {
	return getMongoDB().C(collection)
}

func getMongoDB() *mgo.Database {
	if mongoDatabase == nil {
		if getMongoSession() == nil {
			panic("No Mongo Session Instace")
		}

		mongoDatabase = getMongoSession().DB(database)
	}
	return mongoDatabase
}

func getMongoSession() *mgo.Session {
	if mongoSession == nil {
		initDatabase()
	}

	return mongoSession
}
